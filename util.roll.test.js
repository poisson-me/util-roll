const test = require("tape");
const { randomInt, chanceSuccess, rollDie, rollDice, rollEquation, any, weighted, shuffle, Distribution } = require("./util.roll");

test(function (t) {
  t.plan(9);

  t.ok(randomInt, "randomInt is truthy");
  t.ok(chanceSuccess, "chanceSuccess is truthy");
  t.ok(rollDie, "rollDie is truthy");
  t.ok(rollDice, "rollDice is truthy");
  t.ok(rollEquation, "rollEquation is truthy");
  t.ok(any, "any is truthy");
  t.ok(weighted, "weighted is truthy");
  t.ok(shuffle, "shuffle is truthy");
  t.ok(Distribution, "Distribution is truthy");
}, "can import functions");

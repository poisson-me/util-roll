# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Basic test verifying imports

### Changed
- Functions are exported using CommonJS

## [0.0.1] - 2020-03-26
### Added
- Functions for rolling dice.
- Distribution classes for choosing random values from an array.

[Unreleased]: https://gitlab.com/poisson-me/util-roll/compare/v0.0.1...master
[0.0.1]: https://gitlab.com/poisson-me/util-roll/-/tags/v0.0.1

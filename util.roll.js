const rng = Math.random; // just use basic RNG until there's a need for proper seeding

const sumReducer = (a, b) => a + b;
const times = (n) => Array(n).fill(1);

/**
 * Generates a random integer from min (inclusive) to max (exclusive).
 * If min is not provided, defaults to 0.
 * ex. randomInt(10) => [0 - 9]
 * ex. randomInt(7, 1) => [1 - 6]
 * @return integer
 */
const randomInt = (max, min) => {
  min = min || 0;
  return Math.floor(rng() * (max - min)) + min;
};

/**
 * Returns true chance% of the time.
 * Provide a number between 1 and 100.
 * @return boolean
 */
const chanceSuccess = (chance) => {
  return randomInt(100) < chance;
};

/**
 * Generates a random integer from 1 (inclusive) to the number of sides (inclusive)
 * This simulates the roll of a single die.
 * @return integer
 */
const rollDie = (sides) => {
  return randomInt(sides + 1, 1);
};

/**
 * Rolls a single die type N times and returns the sum.
 *
 * Die string is two integers delimited by the character 'd', ex. 2d6 d20 1d20
 * If either integer is not provided, it's assumed to be 1.
 * If no d is provided, number of sides is assumed to be 1, essentially returning an integer.
 * ex. d20 => rollDie(20)
 * ex. 1d20 => rollDie(20)
 * ex. 3d6 => rollDie(6) + rollDie(6) + rollDie(6)
 * ex. 2 => rollDie(1) + rollDie(1)
 * @return integer
 */
const rollDice = (string) => {
  let [countStr, sidesStr] = string.split("d");
  let count = parseInt((countStr || "").trim()) || 1;
  let sides = parseInt((sidesStr || "").trim()) || 1;
  return times(count).map(() => rollDie(sides)).reduce(sumReducer);
};

/**
 * Rolls an equation of dice and integers, delimited by addition.
 * Empty spaces are trimmed.
 *
 * ex. 1d20 + 3
 * @return integer
 */
const rollEquation = (string) => {
  let parts = string.split("+");
  return parts.map(part => part.trim())
    .map(part => part.length ? rollDice(part) : 0)
    .reduce(sumReducer)
};

/**
 * Returns a random item from the array of choices
 * @return a random item from the array
 */
const any = (choices) => {
  return choices[randomInt(choices.length)];
};

class RandomDistribution {
  constructor (choices) {
    this.choices = choices;
  }

  get () {
    return any(this.choices);
  }
}

class WeightedDistribution {
  constructor (choices) {
    this.choices = choices;
    this.weight = choices.map(o => o[0]).reduce(sumReducer);
  }

  get () {
    let roll = 1 + randomInt(this.weight);

    let sum = 0, index = 0;
    for (index = 0; index < this.choices.length - 1; index ++) {
        sum += this.choices[index][0];
        if (roll <= sum) {
            break;
        }
    }

    return this.choices[index][1];
  }
}

/**
 * Returns a random item from the array of choices which are individually weighted.
 * Each item in the array is expected to be an array of 2 values, [weight, item].
 * Weights don't need to sum to any particular number, although choosing a number like 10 or 100 may help visualize
 *     percentage chance of any once item being chosen.
 * ex. [ [10, "a"], [1, "b"], [5, "c"] ]
 *     "a" is 10 times more likely to be picked over "b" and 2 times more likely than "c".
 * @return a random item from the array
 */
const weighted = (choices) => {
  return new WeightedDistribution(choices).get();
};

/**
 * Returns a shuffled copy of the provided array.
 *
 * Inside-out variant of the Fisher-Yates shuffle
 * https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
 * This variant produces a shuffled copy without modifying the source
 */
const shuffle = (items) => {
  var j = 0;
  let shuffled = [];
  for (var i = 0; i < items.length; i++) {
      j = randomInt(i);
      if (i !== j) {
          shuffled[i] = shuffled[j];
      }
      shuffled[j] = items[i];
  }
  return shuffled;
};

class ShuffleDistribution {
  constructor (choices) {
    this.currentIndex = 0;
    this.choices = shuffle(choices);
  }

  get () {
    let previousIndex = this.currentIndex++;
    this.currentIndex = this.currentIndex % this.choices.length;
    return this.choices[previousIndex];
  }
}

class Distribution {
  constructor (name, choices) {

    switch (name) {
      case "weighted":
        this.distribution = new WeightedDistribution(choices);
        break;
      case "shuffle":
        this.distribution = new ShuffleDistribution(choices);
        break;
      case "random":
      default:
        this.distribution = new RandomDistribution(choices);
    }
  }

  get () {
    return this.distribution.get();
  }

  getN (count) {
    return times(count).map(() => this.get());
  }
}

exports.randomInt = randomInt;
exports.chanceSuccess = chanceSuccess;
exports.rollDie = rollDie;
exports.rollDice = rollDice;
exports.rollEquation = rollEquation;
exports.any = any;
exports.weighted = weighted;
exports.shuffle = shuffle;
exports.Distribution = Distribution;
